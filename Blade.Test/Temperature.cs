﻿namespace Blade.Test
{
    /// <summary>
    ///     Temperature enum default values in c# are always 0 or first enum element
    /// </summary>
    internal enum Temperature
    {
        Unknown,
        Low,
        Medium,
        High
    };
}